<?php 
get_header(); ?>
<?php
if(have_posts()):
	echo '<artile '; post_class( ); echo '>';
	while (have_posts()):
		the_post();?>
		<div class="container">
			<h3 class="post-title"><?php the_title(); ?></h3>
			<figure class="featured-image"><?php echo get_the_post_thumbnail(get_the_id(), 'abc-events-slider'); ?></figure>
			<div class="post-content">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile;
	echo '</artile>';
endif;
$exclude = get_the_id();
$postType = get_post_type($exclude);
wp_reset_query();
$args = array(
	'post_type' => $postType,
	'showposts' => 5,
	'post__not_in' => array($exclude)
);
$query = new WP_Query($args);
if($query->have_posts()):
	echo '<div class="container">';
	echo '<div class="related-posts owl-carousel">';
	while($query->have_posts()):
		$query->the_post();
		echo '<div class="slides">';
			echo '<figure class="post-image"><a href="' . get_the_permalink() . '">' . get_the_post_thumbnail(get_the_id(), 'abc-related-post').'</a></figure>';
			echo '<h2><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h2>';
			the_excerpt();
			echo '<div class="post-date"><i class="icon-calendar"></i>' . get_the_date() . '</div>';
		echo '</div>';
	endwhile;
	echo '</div>';
	echo '</div>';
endif;
?>
<?php get_footer(); ?>