<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<?php function add_menu_link_class($atts, $item, $args)
{
    $atts['class'] = 'nav-link';
    return $atts;
}
add_filter('nav_menu_link_attributes', 'add_menu_link_class', 1, 3); ?>
<body <?php body_class(); ?>>

	<?php
	wp_body_open();
	?>

	<header>
		<div class="backdrop"></div>
		<div class="navbar navbar-dark fixed-top">
			<div class="container-lg d-flex justify-content-between">
				<a href="<?php echo get_option('home') ?>" class="navbar-brand">
					<?php $logo = ot_get_option('logo'); 
					if( $logo ) echo '<img src="'.$logo.'" alt="'.get_bloginfo('name').'" />';
					else '<h5 class="">'.get_bloginfo('name').'</h5>';
					?>		
				</a>
				<style>
			
@media screen and (max-width:767px ){
	.navbar a.navbar-brand img {

max-width: 90px;
}
.mobile-header{
	position: relative;
    top: 7px;
}
.chairman-desk{
	margin-top: 0 !important ;
}
}
				</style>
				<div class="mobile-header">
					<div class="dropdown">
<!-- 						<button class="globe" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&nbsp;Countries</button>
						<div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="dropdownMenuButton">
							<nav class="navbar">
								<ul class="navbar-nav">
								    <?php wp_nav_menu(array(
										'theme_location' => 'countries',
										'menu_id' => 'countries',
										'container' => '',
										'items_wrap' => '%3$s'
									)); ?>
								</ul>
							</nav>
						</div> -->
						   <div class="countries text-center">
              <div class="icon" id="country-icon" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/world.svg)"></div>
              <ul class="country_list">
              
				   <?php wp_nav_menu(array(
										'theme_location' => 'countries',
										'menu_id' => 'countries',
										'container' => '',
										'items_wrap' => '%3$s'
									)); ?>
              </ul>
            </div>
					</div>
					<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span class="white-bar"></span>
						<span></span>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="navbarHeader">
					<ul class="navbar-nav ml-auto">
						<?php wp_nav_menu(array(
							'theme_location' => 'primary',
							'menu_id' => 'primary',
							'container' => '',
							'items_wrap' => '%3$s'
						)); ?>
					</ul>
				</div>
			</div>
		</div>
	</header>


	<?php
		// Output the menu modal.
	get_template_part( 'template-parts/modal-menu' );
