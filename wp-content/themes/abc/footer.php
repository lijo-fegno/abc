			<footer id="site-footer" role="contentinfo" class="header-footer-group">

				<div class="section-inner">

					<div class="container">
						<div class="footer-widgets">
							<div class="row">
								<div class="col-lg-4 col-md-4">
									<?php dynamic_sidebar( 'sidebar-1' ); ?>
								</div>
								<div class="col-lg-3 col-md-3">
									<?php dynamic_sidebar( 'sidebar-2' ); ?>
								</div>
								<div class="col-lg-3 col-md-3">
									<?php dynamic_sidebar( 'sidebar-3' ); ?>
								</div>
								<div class="col-lg-2 col-md-2">
									<?php dynamic_sidebar( 'sidebar-4' ); ?>
								</div>
							</div>
						</div>
					</div>

					<div class="footer-credits dark-bg">
						<div class="container">
							<div class="row">
								<div class="col-md-6 col-lg-6">
									<p class="footer-copyright">
										<?php echo ot_get_option('copyright'); ?>
									</p><!-- .footer-copyright -->
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="social-media-links">
										<?php $sm = ot_get_option('social_media_links');
										foreach( $sm as $s ){
											if( ! empty( $s['href'] ) )
												echo '<a href="' . $s['href'] . '" class="' . $s['title'] . '"><span class="icon-' . $s['title'] . '"></span></a>';
										} ?>
									</div>
								</div>
							</div>
						</div>						

					</div><!-- .footer-credits -->

				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.4/jquery.sticky.min.js" integrity="sha512-QABeEm/oYtKZVyaO8mQQjePTPplrV8qoT7PrwHDJCBLqZl5UmuPi3APEcWwtTNOiH24psax69XPQtEo5dAkGcA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<script>
 window.jQuery(document).ready(function(){
    window.jQuery(".sticky-header-abc").sticky({topSpacing:0});
  });
</script>
<script>
 window.jQuery(document).ready(function(){
    window.jQuery(".sticky-header-abc").sticky({topSpacing:0});
 });
 (function($){
  var list = $(".country_list").attr("aria-expanded","false");
	 list.find("li").each(function(){
       $(this).attr("data-country",$(this).text().toLowerCase());
	   var text = $(this).find("a").text() ;
	   $(this).find("a").html($("<span></span>").text(text));
  });
  var totalDelay = 860  , timer = null , isInitiallyOpened = false ;
  $("#country-icon").bind("click",function(){
	  var menu = $('[data-target="#navbarHeader"]') ;
      timer && clearTimeout(timer);
     var isOpen = list.attr("aria-expanded") === "true" ;
     var _screen = $(window).width() ;
     if(!isOpen){
		 menu.attr("aria-expanded") === "true" &&  menu.trigger("click");
        list.css({
          display : "flex"
        });
        setTimeout(function(){
          list.attr("aria-expanded","true");
        });
     }else{
       list.attr("aria-expanded","false");
       timer = setTimeout(function(){
          list.hide();
       },  _screen > 1169 ? totalDelay  : 0 );
     }
     !isInitiallyOpened && setTimeout(function(){
        isInitiallyOpened = true ;
        $(window).bind("resize",function(){
            if(list.attr("aria-expanded") !=="true"){
                return ;
            }
           var screen = $(window).width() ;
           if(screen < 1170){
              list.attr("aria-expanded","false");
              list.hide();
           }
        })
     }, totalDelay );
  })
  $(window).bind("click",function(e){
	var _screen = $(window).width() ;
	if(_screen > 1169 ){
		return ;
	}
    var isMenuOpen = list.attr("aria-expanded") === "true"  ,
        target = $(e.target),
        meun_wrapper = $(".countries") ;
     if(isMenuOpen){
        if( target.parent(".countries").length > 0 ){
        }else{
          $("#country-icon").trigger("click");
        }
     }
  })
  var screen = $(window).width() ;
  screen > 1169 && $("#country-icon").trigger("click");
 })(window.jQuery);
</script>
	</body>
</html>
