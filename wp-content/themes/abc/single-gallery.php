<?php 
get_header(); ?>
<?php
if(have_posts()):
	echo '<artile '; post_class( ); echo '>';
	while (have_posts()):
		the_post();?>
		<div class="container">
			<h3 class="post-title"><?php the_title(); ?></h3>
			<!-- <figure class="featured-image"><?php //echo get_the_post_thumbnail(get_the_id(), 'abc-events-slider'); ?></figure> -->
			<div class="text-center">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile;
	echo '</artile>';
endif;
$exclude = get_the_id();
$postType = get_post_type($exclude);
wp_reset_query();
$args = array(
	'post_type' => $postType,
	'showposts' => 5,
	'post__not_in' => array($exclude)
);
$query = new WP_Query($args);
if($query->have_posts()):
	$count = 1;
	echo '<div class="abc-related-posts">';
	echo '<div class="container">';
	echo '<h3>' . __('Similar Videos') . '</h3>';
	echo '<div class="row">';
	while($query->have_posts()):
		$query->the_post();
		$cssClass = ( $count == 1 ) ? 'col-md-12' : 'col-md-6 col-lg-6 col-sm-6';
		$imgSize = ( $count == 1 ) ? 'abc-related-gallry-l' : 'abc-related-gallry-sm';
		echo '<div class="' . $cssClass . '">';
			echo '<figure class="post-image post-image-'.$count.'">
					<a href="' . get_the_permalink() . '">' . get_the_post_thumbnail( get_the_id(), $imgSize ).'
					<span class="icon-play"></span>
					</a>
				</figure>';
		echo '</div>';
		$count++;
	endwhile;
	echo '</div>';
	echo '</div>';
	echo '</div>';
endif;
?>
<?php get_footer(); ?>