<?php get_header(); ?>

<?php
if(have_posts()): ?>
	<article <?php post_class();?>>
	<?php while (have_posts()):
		the_post();?>
		<div class="container">
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
		</div>
	<?php endwhile; ?>
	</article><?php
endif;
?>

<?php get_footer(); ?>