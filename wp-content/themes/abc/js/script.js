jQuery(document).ready(function($){

	// Hide Header on on scroll down
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('header .navbar').outerHeight();

	$(window).scroll(function(event){
	    didScroll = true;
	});

	setInterval(function() {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 250);

	function hasScrolled() {
	    var st = $(this).scrollTop();
	    
	    // Make sure they scroll more than delta
	    if(Math.abs(lastScrollTop - st) <= delta)
	        return;
	    
	    // If they scrolled down and are past the navbar, add class .nav-up.
	    // This is necessary so you never see what is "behind" the navbar.
	    if (st > lastScrollTop && st > navbarHeight){
	        // Scroll Down
	        $('header .navbar').removeClass('nav-down').addClass('nav-up');
	    } else {
	        // Scroll Up
	        if(st + $(window).height() < $(document).height()) {
	            $('header .navbar').removeClass('nav-up').addClass('nav-down');
		        if (st <= 50) {
			        $('header .navbar').removeClass('nav-down');
			    }
	        }
	    }
	    
	    lastScrollTop = st;
	}

	$('.navbar-toggler').click(function(){
		$('.backdrop').toggleClass('show');
	});

	// Select all links with hashes
	$('a[href*="#"]')
	  // Remove links that don't actually link to anything
	  .not('[href="#"]')
	  .not('[href="#0"]')
	  .click(function(event) {
	    // On-page links
	    if (
	      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
	      && 
	      location.hostname == this.hostname
	    ) {
	      // Figure out element to scroll to
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	      // Does a scroll target exist?
	      if (target.length) {
	        // Only prevent default if animation is actually gonna happen
	        event.preventDefault();
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 400, function() {
	          // Callback after animation
	          // Must change focus!
	          var $target = $(target);
	          $target.focus();
	          if ($target.is(":focus")) { // Checking if the target was focused
	            return false;
	          } else {
	            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
	            $target.focus(); // Set focus again
	          };
	        });
	      }
	    }
	  });

	//var rellax = new Rellax('.abt-sec-img .vc_single_image-wrapper');

	$('.blog-posts').owlCarousel({
    	stagePadding: 100,
	    loop:true,
	    margin:62,
	    nav:true,
	    dots: false,
	    center:true,
		autoplay : true ,
		autoplayTimeout:3000 ,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:2
	        }
	    }
	});

	$('.teams').owlCarousel({
		stagePadding: 100,
	    loop: true,
	    margin:62,
	    nav:true,
	    dots: false,
	    center: true,
	    autoWidth:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('.related-posts').owlCarousel({
	    loop: false,
	    margin:50,
	    nav: false,
	    dots: false,
	    autoplay:true,
	    autoplayTimeout:4000,
	    autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1,
	            dots: true
	        },
	        576:{
	            items:2
	        },
	        1000:{
	            items:3,
	            dots: false
	        }
	    }
	});

	$('select[name=country]').change(function(){
		var cnt = $(this).val();
		$('.address').hide();
		$('.address.address-'+cnt).show().trigger("resize");
	});

	$('.show-map .vc_btn3').click(function(e){
		e.preventDefault();
		$(this).parents('.map-container-block').find('.map-container').slideDown();
	});

});

jQuery(window).on('load resize', function(){
	var imgWidth = jQuery('.flip-card img').width();
	var imgHeight = jQuery('.flip-card img').height();
	jQuery('.flip-card').css({
		'width': imgWidth,
		'height': imgHeight
	});
});

jQuery(function($) {
	if($('.map-slider').length){
	    $(window).on('scroll', function() {
	    	if( ! $('.map-slider').hasClass('abc-animated') ){
		        if($(window).scrollTop() >= $('.section-map').offset().top) {
		        	$('.map-slider').addClass('abc-animated');
		        	setTimeout(function(){
		        		$('.location.china').addClass('wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown wpb_start_animation animated');
		        		$('.map-dot.china').css('opacity', 1);
		        	}, 10);
		        	setTimeout(function(){
		        		$('.location.qatar').addClass('wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown wpb_start_animation animated');
		        		$('.map-dot.qatar').css('opacity', 1);
		        		$('.map-slider-progress').width('33.33%');
		        	}, 1000);
		        	setTimeout(function(){
		        		$('.location.oman').addClass('wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown wpb_start_animation animated');
		        		$('.map-dot.oman').css('opacity', 1);
		        		$('.map-slider-progress').width('66.66%');
		        	}, 1500);
		        	setTimeout(function(){
		        		$('.location.tanzania').addClass('wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown wpb_start_animation animated');
		        		$('.map-dot.tanzania').css('opacity', 1);
		        		$('.map-slider-progress').width('100%');
		        	}, 2000);
		        }
		    }
	    });
	}
});